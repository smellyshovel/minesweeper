class Cell {
  constructor(opts) {
    this.opts = opts;
    this.field = opts.field;
    this.minesAround = 0;
  }

  get neighbors() {
    let neighbors = [];

    for (var x = this.opts.numX - 1; x <= this.opts.numX + 1; x++) {
      for (var y = this.opts.numY - 1; y <= this.opts.numY + 1; y++) {
        let cell = this.field.cell(x, y);
        if (cell && cell != this && !cell.hasMine)
          neighbors.push(cell);
      }
    }

    return neighbors;
  }

  set hasMine(v) {
    this.opts.mine = v;
  }

  get hasMine() {
    return !!this.opts.mine;
  }
}

class Field {
  constructor(opts) {
    this.opts  = opts;
    this.cells = [];
  }

  cell(numX, numY) {
    let neededCell;

    this.cells.find((cell) => {
      if (cell.opts.numX == numX && cell.opts.numY == numY) {
        neededCell = cell;
      }
    });

    return neededCell;
  }
}

class Game {
  constructor(opts) {
    this.opts = opts;

    this.initializeCanvas();
    this.initializeField();
    this.initializeCells();
  }

  initializeCanvas() {
    let canvasWidth  = this.opts.field.sizeX * (this.opts.cell.size + this.opts.field.cw * 2);
    let canvasHeight = this.opts.field.sizeY * (this.opts.cell.size + this.opts.field.cw * 2);
    this.CANVAS = document.querySelector("#game");
    this.CANVAS.width  = canvasWidth;
    this.CANVAS.height = canvasHeight;

    this.CONTEXT = this.CANVAS.getContext("2d");
  }

  initializeField() {
    this.field = new Field({
      sizeX: this.opts.field.sizeX,
      sizeY: this.opts.field.sizeY
    });
  }

  initializeCells() {
    for (let numX = 0; numX < this.opts.field.sizeX; numX++) {
      for (let numY = 0; numY < this.opts.field.sizeY; numY++) {
        let opts = {
          field: this.field,
          mine: Math.random() < .1 ? 1 : 0,
          numX: numX,
          numY: numY,
          size: this.opts.cell.size,
          cw: this.opts.cell.cw
        };

        let cell = new Cell(opts);
        this.field.cells.push(cell);

        if (cell.hasMine) {
          cell.neighbors.forEach((neighbor) => {
            neighbor.minesAround++;
          });
        }
      }
    }
  }
}

const GAME = new Game({
  field: {
    sizeX: 16,
    sizeY: 16,
  },

  cell: {
    size: 24,
    cw: 4 // clearwhite
  }
});

// GAME.CANVAS.addEventListener("click", (event) => {
//
// });

// console.log(GAME.field.cell(0, 0).field.cell(15, 15));
