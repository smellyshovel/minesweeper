class Cell {
  constructor(field, x, y) {
    this.field = field;
    this.numX  = x;
    this.numY  = y;
    this.opts  = this.field.game.opts.cell;

    this.hasMine     = false;
    this.minesAround = 0;
    this.opened      = false;
    this.flagged     = false;
  }

  static nbs(targetCell) {
    let nbs = [];

    for (let x = targetCell.numX - 1; x <= targetCell.numX + 1; x++) {
      for (let y = targetCell.numY - 1; y <= targetCell.numY + 1; y++) {
        let cell = targetCell.field.cell(x, y);
        if (cell && cell != targetCell && !cell.hasMine) {
          nbs.push(cell);
        }
      }
    }

    return nbs;
  }

  get top() {
    return this.numY * this.opts.size + this.opts.cw * this.numY;
  }

  get bottom() {
    return this.numY * this.opts.size + this.opts.cw * this.numY + this.opts.size;
  }

  get left() {
    return this.numX * this.opts.size + this.opts.cw * this.numX;
  }

  get right() {
    return this.numX * this.opts.size + this.opts.cw * this.numX + this.opts.size;
  }

  open() {
    if (!this.flagged) { // player can't open flagged cell
      if (this.hasMine) { // if cell to open has mine, open this cell and end the game
        this.opened = true;
        this.field.game.over();
      } else { // if cell hasn't mine
        let openRelated = (cell) => {
          cell.opened = true;

          let related = Cell.nbs(cell);
          while (related.length) {
            let cellx = related.shift();

            if (!cellx.minesAround && !cellx.opened) {
              cellx.opened = true;
              related = related.concat(Cell.nbs(cellx));
            } else if (cellx.minesAround) {
              cellx.opened = true;
            }
          }
        };

        if (this.minesAround) { // if cell to open has mines around it
          this.opened = true;
        } else { // if cell to open is absolutely clear, open it with related to it
          openRelated(this);
        }
      }
    }
  }

  flag() {
    if (!this.flagged) { // if not flagged yet
      if (!this.opened) {
        this.flagged = true;
      }
    } else { // if flagged already
      this.flagged = !this.flagged;
    }
  }

  get pattern() {
    if (!this.opened) {
      if (this.flagged) {
        return {
          bg: "#ccc",
          color: "#000",
          content: "⚑"
        };
      } else {
        return {
          bg: "#ccc",
          content: null
        };
      }
    } else {
      if (this.hasMine && this.flagged) {
        return {
          bg: "#ccc",
          color: "#000",
          content: "⚑"
        };
      } else if (this.hasMine) {
        return {
          bg: "#f2f2f2",
          color: "#f00",
          content: "💣"
        };
      } else if (this.minesAround) {
        return {
          bg: "#f2f2f2",
          color: "#000",
          content: this.minesAround
        };
      } else {
        return {
          bg: "#f2f2f2",
          content: null
        }
      }
    }
  }

  draw() {
    let context = this.field.game.CONTEXT;

    context.fillStyle = this.pattern.bg;
    context.fillRect(
      this.left,
      this.top,
      this.opts.size,
      this.opts.size
    );

    if (this.pattern.content) {
      context.font = "26px Unifont"
      context.fillStyle = this.color || this.pattern.color;
      context.fillText(
        this.pattern.content,
        this.left,
        this.top + this.opts.size
      );
    }
  }
}

class Field {
  constructor(game) {
    this.game = game;
    this.opts = this.game.opts.field;

    this.cellsWithMines = [];
  }

  get flaggedCells() {
    return this.cells.filter((cell) => {
      if (cell.flagged) {
        return cell;
      }
    });
  }

  get closedCells() {
    return this.cells.filter((cell) => {
      if (!cell.opened) {
        return cell;
      }
    });
  }

  initializeCells() {
    this.cells = [];

    for (let x = 0; x < this.opts.sizeX; x++) {
      for (let y = 0; y < this.opts.sizeY; y++) {
        this.cells.push(new Cell(this, x, y));
      }
    }
  }

  fillWithMines(excludedCellIndex) {
    let minesPlaced = 0, size = this.opts.sizeX * this.opts.sizeY;

    while (minesPlaced < this.opts.mines) {
      let cellForMinePlacingIndex = Math.random() * size | 0,
          cellForMinePlacing = this.cells[cellForMinePlacingIndex],
          nbsOfExcludedCell = Cell.nbs(this.cells[excludedCellIndex]);

      if (!cellForMinePlacing.hasMine && cellForMinePlacingIndex != excludedCellIndex) {
        if (!nbsOfExcludedCell.includes(cellForMinePlacing)) {
          cellForMinePlacing.hasMine = true;
          minesPlaced++;

          this.cellsWithMines.push(cellForMinePlacing);

          Cell.nbs(cellForMinePlacing).forEach((nb) => {
            nb.minesAround++;
          });
        }
      }
    }
  }

  cell(x, y) {
    return this.cells.find((cell) => {
      if (cell.numX == x && cell.numY == y) {
        return cell;
      }
    });
  }

  draw() {
    this.cells.forEach((cell) => {
      cell.draw();
    });
  }
}

class Game {
  constructor(opts) {
    this.opts = opts;
    this.status = 0;
  }

  initializeCanvas() {
    this.CANVAS = document.querySelector("#game");

    this.CANVAS.width  = this.opts.field.sizeX * this.opts.cell.size + this.opts.cell.cw * (this.opts.field.sizeX - 1);
    this.CANVAS.height = this.opts.field.sizeY * this.opts.cell.size + this.opts.cell.cw * (this.opts.field.sizeY - 1);

    this.CONTEXT = this.CANVAS.getContext("2d");
  }

  initializeField() {
    this.field = new Field(this);
  }

  handleClick(mouseButton, x, y) {
    let findCell = (callback) => {
      this.field.cells.find((cell, i) => {
        if (cell.left < x && cell.right > x && cell.top < y && cell.bottom > y) {
          if (cell) callback(cell, i);
        }
      });
    };

    if (mouseButton === "left") {

      if (this.status == 0) { // the game is not started
        findCell((cell, i) => {
          console.log(cell.hasMine);
          this.field.fillWithMines(i);
          cell.open();

          this.status = 1;
        });
      } else if (this.status == 1) { // the game is on
        findCell((cell) => {
          console.log(cell.hasMine);
          cell.open();
        });
      }

      this.checkForWin();

    } else if (mouseButton === "right") {

      if (this.status == 1) {
        findCell((cell) => {
          cell.flag();
        });
      }

    }

    this.field.draw();
  }

  checkForWin() {
    let res;
    if (this.field.closedCells.length == this.field.cellsWithMines.length) {
      for (cellWithMine of this.field.cellsWithMines) {
        if (!this.field.closedCells.includes(cellWithMine)) {
          res = false;
        }
      }
    }

    if (res) {
      alert("Won");
    }
  }

  over() {
    this.field.cellsWithMines.forEach((cell) => {
      cell.opened = true;
    });

    this.status = 2;
  }
}

const GAME = new Game({
  field: {
    mines: 6,
    sizeX: 16,
    sizeY: 16
  },

  cell: {
    size: 24,
    cw: 8
  }
});

GAME.initializeCanvas();
GAME.initializeField();
GAME.field.initializeCells();
GAME.field.draw();

GAME.CANVAS.addEventListener("click", (event) => {
  GAME.handleClick("left", event.offsetX, event.offsetY);
});

GAME.CANVAS.addEventListener("contextmenu", (event) => {
  GAME.handleClick("right", event.offsetX, event.offsetY);
  event.preventDefault();
});

// console.log(GAME.field.game.field.game.field.game.field.cell(13, 2).field.game.field.cell(2, 2));
